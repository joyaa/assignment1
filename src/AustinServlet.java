
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.annotations.SerializedName;


public class AustinServlet extends HttpServlet  {

	static class Link {
		String url;
		
		public String getLink(){
			return url;
		}
		
	}
	static class Element {
		@SerializedName("owner_department")
		String ownerDepartment;
		
		@SerializedName("account")
		String account;
		
		@SerializedName("type")
		String type;
		
		public String getAccount() {
			return account;
		}
		
		public String getType() {
			return type;
		}
		
		@Override
	    public String toString() {
	        return "Element [owner_department=" + ownerDepartment + ", account=" + account
	                + ", type=" + type + "]";
	    }  
	}
	static class UrlElement extends Element {
		@SerializedName("external_link")	
		Link url;
		
		public String getLink() {
			return url.getLink();
		}
		
		@Override
	    public String toString() {
	        return super.toString() + ", link="+url.getLink()+"]";
	    }  
	}

	static class ActivityElement extends UrlElement {
		@SerializedName("incoming_percentage")
		String incomingPercentage;
		
		@SerializedName("incoming_activity")
		String incomingActivity;
		
		@SerializedName("total_activity")
		String totalActivity;
		
		@SerializedName("outgoing_activity")
		String outgoingActivity;
		
	}
	   
    Map<String, String> userData;
    Map<String, String> loggedInUsers;
    Integer uniqueId = 0;
	Cookie[] cookies;
	boolean sessionOn;
	String user;
    
	/**
	 * 
	 */ 
	private static final long serialVersionUID = 1L;
	
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) 
                    throws ServletException, IOException
    {
		//url to get json 
		final String urlString = "https://www.cs.utexas.edu/~devdatta/ej42-f7za.json";
		URL url = new URL(urlString);
		BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
		StringBuffer buffer = new StringBuffer();
		int read;
		char[] chars = new char[1024];
		while((read = reader.read(chars)) != -1)
			buffer.append(chars, 0, read);
		//response.getWriter().println(buffer);
		if (reader != null)
			reader.close();
		String json = buffer.toString();
		
		//parsing the json-string array
		ArrayList<UrlElement> elements = new ArrayList<UrlElement>();
		ArrayList<Element> restElements = new ArrayList<Element>(); //those without links
		JsonParser parser = new JsonParser();
	    JsonArray Jarray = parser.parse(json).getAsJsonArray();
	    Gson gson = new Gson();
	    for(JsonElement obj : Jarray){
	    	ActivityElement activityElement;
			if (obj.getAsJsonObject().get("incoming_percentage") != null){
				   activityElement = gson.fromJson(obj, ActivityElement.class);
				   elements.add(activityElement);
			} else if (obj.getAsJsonObject().get("external_link") != null) {
				   UrlElement urlElement = gson.fromJson(obj, UrlElement.class);
				   elements.add(urlElement);
			} else {
				   Element element = gson.fromJson(obj, Element.class);
			        restElements.add(element);
			}
	    }        
		
		//read parameters
    	String accountName = request.getParameter("account_name");
        //response.getWriter().println("Account Name:" + accountName);
        
        String type = request.getParameter("type");
        //response.getWriter().println("Type:" + type);
        
        String username = request.getParameter("username");
        String session = request.getParameter("session");
        
        if(accountName != null) {
           	response.getWriter().println("Visited URLs");
           	
           	if(sessionOn)
           		printAddCookies(request, response);
           	
	        //process parameters - query
	        response.getWriter().println("\nURL DATA");
	        boolean urlFound = false;
	        for (UrlElement element : elements) {
		        if(type != null) {
	        		if (element.account.toLowerCase().equals(accountName.toLowerCase()) && element.type.toLowerCase().equals(type.toLowerCase())) {
	        			response.getWriter().println(element.getLink());
	        			urlFound = true;
	        		}
		        } else {
		        	if (element.account.toLowerCase().equals(accountName.toLowerCase())) {
		        		response.getWriter().println(element.getLink());
		        		urlFound = true;
		        	}
		        } 
	        }
	
	        //entries without url
	        if (!urlFound) {
	        	for (Element element : restElements ) {
	        		if(type != null) {
	            		if (element.account.toLowerCase().equals(accountName.toLowerCase()) && element.type.toLowerCase().equals(type.toLowerCase())) {
	            			response.getWriter().println("url not found for request");
	            			urlFound = true;
	            		}
	    	        } else {
	    	        	if (element.account.toLowerCase().equals(accountName.toLowerCase())) {
	    	        		response.getWriter().println("url not found for request");
	    	        		urlFound = true;
	    	        	}
	    	        } 
	        	}
	        }
	        if (!urlFound)
	        	response.getWriter().println("invalid parameters");
        } else if (accountName == null) {
            SessionHandler(request, response, session, username);
        }
    }
	

	
    protected void SessionHandler(HttpServletRequest request, HttpServletResponse response, String session, String username) 
                    throws ServletException, IOException
    {       
    	cookies = request.getCookies();
   		if(session != null && session.equals("start") && !sessionOn) {
			sessionOn = true;
			response.getWriter().println("Session started by user: "+username);
			user = username;
			emptyCookies(request, response);
		} else if (session.equals("end") && user.equals(username) ){
    		emptyCookies(request, response);
            response.getWriter().println("Session ended.");
            sessionOn = false;
            username = null;
            uniqueId = 0;
    	} else if (session.equals(null)){
    		printAddCookies(request, response);
    	}
    }   
    
    private void printAddCookies(HttpServletRequest request, HttpServletResponse response) throws IOException {
    	cookies = request.getCookies();
        for(int i=0; cookies != null && i < cookies.length; i++) {
            Cookie cookie = cookies[i];
            String cookieValue = cookie.getValue();
            response.getWriter().println(cookieValue);
	    }    
	    uniqueId++;
	    Cookie cookie = new Cookie(uniqueId.toString(), "http://localhost:8080/assignment1" + request.getQueryString());
	    cookie.setDomain("localhost");
	    cookie.setPath("/assignment1" + request.getServletPath());
	    cookie.setMaxAge(1000);
	    response.addCookie(cookie);
    }
    
    private void emptyCookies(HttpServletRequest request, HttpServletResponse response) {
    	if (cookies.length > 0){
	        for(int i=0; cookies != null && i<cookies.length; i++) {
	            Cookie cookie = cookies[i];
	            cookie.setValue(null);
	            cookie.setDomain("localhost");
	            cookie.setPath("/assignment1" + request.getServletPath());
	            cookie.setMaxAge(0);
	            response.addCookie(cookie);
	        }
    	}
    }
    
    
}
